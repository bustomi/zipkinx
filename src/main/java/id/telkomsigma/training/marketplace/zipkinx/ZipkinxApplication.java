package id.telkomsigma.training.marketplace.zipkinx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

import zipkin.server.EnableZipkinServer;

@SpringBootApplication
@EnableZipkinServer
@EnableDiscoveryClient
public class ZipkinxApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZipkinxApplication.class, args);
	}
}
